﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// The name of your plugin
[assembly: AssemblyTitle("Darks Customs")]
// A short description of your plugin
[assembly: AssemblyDescription("Compilation of Sequencer 2 plugins; currently included: Filter Offset Calculator")]
[assembly: AssemblyConfiguration("")]
//Your name
[assembly: AssemblyCompany("S. Dimant & Stefan Berg")]
//The product name that this plugin is part of
[assembly: AssemblyProduct("NINA.Plugin.DarksCustoms")]
[assembly: AssemblyCopyright("Copyright © S.Dimant, Stefan Berg 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is used as a unique identifier of the plugin
[assembly: Guid("9516aaee-7abd-4ca3-9a19-51a86723d4c1")]

//The assembly versioning
//Should be incremented for each new release build of a plugin
[assembly: AssemblyVersion("1.2.1.0")]
[assembly: AssemblyFileVersion("1.2.1.0")]

//The minimum Version of N.I.N.A. that this plugin is compatible with
[assembly: AssemblyMetadata("MinimumApplicationVersion", "1.11.0.1167")]

//Your plugin homepage - omit if not applicaple
[assembly: AssemblyMetadata("Homepage", "https://bitbucket.org/rootdarkarchon/nina.plugin.darkscustoms/src")]
//The license your plugin code is using
[assembly: AssemblyMetadata("License", "MPL-2.0")]
//The url to the license
[assembly: AssemblyMetadata("LicenseURL", "https://www.mozilla.org/en-US/MPL/2.0/")]
//The repository where your pluggin is hosted
[assembly: AssemblyMetadata("Repository", "https://bitbucket.org/rootdarkarchon/nina.plugin.darkscustoms/")]
[assembly: AssemblyMetadata("ChangelogURL", "https://bitbucket.org/rootdarkarchon/nina.plugin.darkscustoms/src/master/NINA.Plugin.DarksCustoms/NINA.Plugin.DarksCustoms/Changelog.md")]

//Common tags that quickly describe your plugin
[assembly: AssemblyMetadata("Tags", "Sequencer, Autofocus, Offset")]

//The featured logo that will be displayed in the plugin list next to the name
[assembly: AssemblyMetadata("FeaturedImageURL", "")]
//An example screenshot of your plugin in action
[assembly: AssemblyMetadata("ScreenshotURL", "")]
//An additional example screenshot of your plugin in action
[assembly: AssemblyMetadata("AltScreenshotURL", "")]
[assembly: AssemblyMetadata("LongDescription", @"")]